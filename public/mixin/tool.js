opts.tools = riot.observable();
opts.tools.list = [];
opts.tools.auto = true;
opts.tools.avanced=true;

opts.tools.add = function (tool,i) {
  opts.tools.list[i]=tool;
  tool.id = i;
  opts.tools.trigger('add');
};

opts.tools.select = function (toolid) {
  opts.tools.selected = opts.tools.list[toolid];
  riot.mount('#option', opts.tools.selected.name, opts);
  console.log("changeSelect triger");
  opts.tools.trigger('changeSelect');
};

opts.tools.on('changeSelect', function () {
  console.log("changeSelect handle");
  riot.mount('#active_node', "frame-" + opts.tools.selected.name, opts);
})

opts.tools.setAvanced= function(e){
  self.opts.tools.avanced=e.target.checked;
}

opts.tools.setAuto= function(e){
  self.opts.tools.auto=e.target.checked;
}

opts.tools.setTarget = function (e) {
  console.log("verify event",e,opts.tools.activeNode=== e.target,!opts.tools.activeNode.contains(e.target));
  if(opts.tools.activeNode=== e.target){
    opts.tools.setTargetNoEvent(opts.tools.activeNode.parentNode)
  } else if (!opts.tools.activeNode.contains(e.target)) {
    opts.tools.setTargetNoEvent(e.target)
  }
};

opts.tools.setTargetNoEvent = function (target) {
  opts.tools.target = target;
  target.insertBefore(opts.tools.activeNode, target.children[0]);
  console.log("tools trigger target",target)
  opts.tools.trigger('target',target);
};

opts.tools.downApply = function (e) {
  if(opts.tools.selected.downApply){
    opts.tools.selected.downApply(e);
  }
};

opts.tools.apply = function (e) {
  opts.tools.selected.apply(e);
}

opts.tools.afterApply = function (e) {
  if(opts.tools.selected.afterApply){
    opts.tools.selected.afterApply(e);
  } else {
    opts.util.applyProps(opts.tools.selected.options.css,opts.tools.selected.init);
    opts.fiche.refresh();
  }
};


opts.tools.beforeSave=function(){
  opts.tools.activeNodeParent =opts.tools.activeNode.parentNode;
  opts.tools.activeNodeParent.removeChild(opts.tools.activeNode);
};

opts.tools.afterSave= function(){
  opts.tools.activeNodeParent.insertBefore(opts.tools.activeNode, opts.tools.activeNodeParent.children[0]);
};