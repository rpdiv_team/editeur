opts.util = {};

opts.util.getActiveStyle=function(){
  return opts.fiche.allcss[opts.fiche.mode][opts.tools.target.getAttribute('id')].style;
}

opts.util.init = function (props, initializer) {
  console.log("-------------init---------------------------");
  var style = opts.util.getActiveStyle();
  for (var prop in props.txt) {
    opts.util.initApply('txt', prop, initializer, style, props, function (x) {
      return x;
    });
  }
  for (var prop in props.num) {
    opts.util.initApply('num', prop, initializer, style, props, parseInt);
  }
  for (var prop in props.color) {
    opts.util.initApply('color', prop, initializer, style, props, function (x) {
      if (x == null || x == "") {
        return x;
      }
      return x.substring(1);
    });
  }
}

opts.util.initApply=function(type, prop, init, origin, props, apply) {
  if (origin[prop] == null || origin[prop] == "") {
    props[type][prop] = init[prop];
  } else {
    props[type][prop] = apply(origin[prop]);
  }
}

opts.util.applyProps = function (props, initializer) {
  var style = opts.util.getActiveStyle();
  for (var prop in props.txt) {
    if (prop != 'unite') {
      opts.util.applyProp('txt', prop, initializer, style, props, function (x) {
        return x;
      });
    }
  }
  for (var prop in props.num) {
    opts.util.applyProp('num', prop, initializer, style, props, function (x) {
      return (x + props.txt.unite);
    });
  }
  for (var prop in props.color) {
    opts.util.applyProp('color', prop, initializer, style, props, function (x) {
      return ('#' + x);
    });
  }
}

opts.util.applyProp=function(type, prop, init, attr, props, apply) {
  if (props[type][prop] == init[prop]) {
    attr[prop] = "";
  } else {
    attr[prop] = apply(props[type][prop]);
  }
}
