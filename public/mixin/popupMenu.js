opts.popupMenu = riot.observable();
opts.popupMenu.selected = "";
opts.popupMenu.elem= document.createElement('div');
opts.popupMenu.elem.setAttribute("id", "popup");
document.body.appendChild(opts.popupMenu.elem);

opts.popupMenu.setDisplay = function ( name) {
  opts.popupMenu.selected=name;
  console.log("opts menu popup", opts);
  riot.mount('#popup', name, opts);
};

opts.popupMenu.hideDisplay = function () {
  riot.unmount('#popup', opts.popupMenu.selected, opts);
};