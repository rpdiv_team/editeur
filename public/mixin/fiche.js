opts.fiche = riot.observable();
opts.fiche.active;
opts.fiche.allcss = {
  id: [],
  class: []
};//nodes arrays with class and id name in id
opts.fiche.font = [];
opts.fiche.mode = 'id';

opts.fiche.name={
  number: 0,
  default: "rpdiv_",
  new: function(){
    while( document.getElementById('rpdiv_'+opts.fiche.name.number)){
      console.log("search nextId", document.getElementById('rpdiv_'+opts.fiche.name.number));
      opts.fiche.name.number++;
    }
    return opts.fiche.name.default+ opts.fiche.name.number;
  }
};


opts.fiche.setFontDiv = function (div) {
  opts.fiche.font_div = div;
}

opts.fiche.setStyleElem = function (elem) {
  console.log("style elem",elem);
  opts.fiche.style=elem;
}

opts.fiche.reset = function (css, fontc, name) {
  console.log(css);
  opts.fiche.allcss = {
    id: {},
    class: {}
  };
  opts.fiche.allcss.class.newElem = document.createElement('div');
  opts.fiche.allcss.class.newElem.style.cssText = "background-color: white;height:50px;width:100%;";
  opts.fiche.font = fontc != null ? fontc : [];
  opts.fiche.linkReload();
  opts.fiche.allcss.id.fiche = document.createElement('div');
  if (css) {
    for (var lineId in css) {
      var line = css[lineId];
      console.log('line', line);
      opts.fiche.allcss[line.type][line.name] = document.createElement('div');
      opts.fiche.allcss[line.type][line.name].style.cssText = line.content;
    }
  }
  opts.fiche.name.number=0;
  opts.fiche.name.name= name || 'rpdiv_';
  document.getElementById("fiche").innerHTML = "";
  console.log(opts.fiche.allcss);
  opts.fiche.refresh();
};

opts.fiche.refresh=function(){
  opts.fiche.style.innerHTML=opts.fiche.styleToString();
  console.log("refresh",opts.fiche.style.innerHTML)
};

opts.fiche.styleToString = function () {
  var result = "";
  for (var val in opts.fiche.allcss.id) {
    result = result + '#' + val + '{' + opts.fiche.elemToString('id', val) + '}';
  }
  for (var val in opts.fiche.allcss.class) {
    result = result + '.' + val + '{' + opts.fiche.elemToString('class', val) + '}';
  }
  return result;
}
    
opts.fiche.elemToString = function (mod, id) {
  return opts.fiche.allcss[mod][id].style.cssText;
}

opts.fiche.linkReload = function () {
  opts.fiche.font_div.innerHTML = "";
  for (var id in opts.fiche.font) {
    opts.fiche.font_div.innerHTML = font_div.innerHTML + "<link href='" + link + "' rel='stylesheet' type='text/css'>";
  }
}

opts.fiche.newPolice = function (link) {
  opts.fiche.font.push({
    link: link
  });
  opts.fiche.font_div.innerHTML = opts.fiche.font_div.innerHTML + "<link href='" + link + "' rel='stylesheet' type='text/css'>";
};

opts.fiche.getFileFont = function () {
  return opts.fiche.font;
};

opts.fiche.save = function () {
  var allcss = opts.fiche.allcss;
  var res = {
    css: [],
    font: opts.fiche.font
  };
  console.log("--------------------",opts.fiche.allcss.class);
  if (opts.fiche.allcss.class.newElem == null) {
    res.css.push({
      type: 'class',
      name: 'newElem',
      content: "background-color: white;height:50px;width:100%;"
    });
  } else if (opts.fiche.allcss.class.newElem.style.cssText == null || opts.fiche.allcss.class.newElem.style.cssText == "") {
    opts.fiche.allcss.class.newElem.style.cssText = "background-color: white;height:50px;width:100%;";
  }
  for (var typ in allcss) {
    for (var tmp in allcss[typ]) {
      res.css.push({
        type: typ,
        name: tmp,
        content: allcss[typ][tmp].style.cssText
      });
    }
  }
  
  opts.tools.beforeSave();
  res.html=document.getElementById("fiche").innerHTML;
  console.log("///////////////");
  console.log("allcss.id.newElem", allcss.id);
  console.log(res);
  opts.tools.afterSave();
  return res;
};

opts.fiche.setIdToElem = function (elem, name) {
  if (opts.fiche.allcss.id[name] != null) {
    if (elem.getAttribute('id') == name) {
      return;
    }
    delete opts.fiche.allcss.id[elem.getAttribute('id')];
  }
  elem.setAttribute('id', name);
  opts.fiche.allcss.id[name] = document.createElement('div');
};

opts.fiche.setClassToElem = function (elem, className) {
  var previousClass = elem.getAttribute('class');
  if(previousClass){
    elem.setAttribute('class', previousClass + ' ' + className);
  } else {
    elem.setAttribute('class', className);
  }
  if (opts.fiche.allcss.class[className] == null) {
    opts.fiche.allcss.class[className] = document.createElement('div');
  }
};


opts.fiche.formatSaveTab= function( data){
  opts.fiche.slots = [];
  data.max_slot = data.max_slot ? data.max_slot : 0;
  for(var i=0;i<data.max_slot;i++){
    opts.fiche.slots.push({slot:i,name:'free' });
  }
  console.log("format apply",opts.fiche.slots);
  console.log(data.save);
  for(var save in data.save){
    opts.fiche.slots[data.save[save].slot]=data.save[save];
  }
  console.log("format apply",opts.fiche.slots);
}
  
opts.fiche.loadSlots=function(){
  console.log("load user fiches");
  marmottajax({url: "/editor/userFiches", method: "get"}).then(
    function(data){
      opts.fiche.formatSaveTab( JSON.parse(data));
      opts.fiche.trigger("slotLoaded");
    },
    function(){"errorMsg"}
  );
};
  
opts.fiche.loadSlots();
