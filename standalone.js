//no user with the standalone

var editor = require('./router');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({
  extended: true
}));

editor.load(app,express,{});

console.log(__dirname + '/standAlonePublic');
app.use('/public/ressources/', express.static(__dirname + '/standAlonePublic', { maxAge: 0 }));

app.listen(8989);
console.log("Server start");
