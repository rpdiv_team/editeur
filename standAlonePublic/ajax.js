function ajaxPromise(method,target,data,opt){
    console.log("ajax");
  return new Promise(function(resolve, reject) {
    var xhr_object = null;
    if(!opt){
        opt={};
    }
    if (window.XMLHttpRequest){ // Firefox
      xhr_object = new XMLHttpRequest();
    }else if (window.ActiveXObject){ // Internet Explorer
      xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
    }else{ // XMLHttpRequest non supporté par le navigateur
      reject(Error("ajax not implemented"));
    }
    console.log("on a ",xhr_object);
    var buffer = [];
    for (var name in data) {
        if (!data.hasOwnProperty(name)) {
          continue;
        }
        var value = data[ name ];
        buffer.push(
          encodeURIComponent(name) +
          "=" +
          encodeURIComponent((value == null) ? "" : value)
          );
      }
    data = buffer.join("&").replace(/%20/g, "+");
    console.log(data);
    if(method=="GET" && (data.length>0)){
      target+="?"+data;
    }
    xhr_object.open(method,target,!opt.sync,opt.username,opt.password );
    // Set headers
    if(method=="POST") {
      xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xhr_object.setRequestHeader("Content-length", data.length);
      xhr_object.setRequestHeader("Connection", "close");
    }

    
    for ( var i in opt.headers ) {
      xhr_object.setRequestHeader( i, opt.headers[ i ] );
    }

    xhr_object.onload = function(){
        console.log("status ",xhr_object );
        if(xhr_object.status<400){
          resolve(xhr_object.responseText);
        }else{
          reject(xhr_object.responseText); 
        }
    };
    xhr_object.onerror = function(e){
        console.log("error",e);
        reject(Error("ajax error"));
    };

    try {
      xhr_object.send(data);
      console.log("send");
      if(opt.sync){
        resolve(xhr_object.responseText);
      }
    } catch (e){
        reject(e);
    }
  });
}
