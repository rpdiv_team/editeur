module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      minScript: {
        files: {
          'public/script/embeddedGraph.min.js': ['public/script/embeddedGraph.js']
        }
      }
    },
    includereplace: {
      docfr: {
        options: {
          docroot: 'public/doc/fr/'
        },
        files: [
          {src: 'doc/fr/index.html', dest: 'public/'},
          {src: 'doc/fr/quickStart.html', dest: 'public/'},
          {src: 'doc/fr/tools/*.html', dest: 'public/'}
        ]
      }
    }
  });

  // Load grunt mocha task
  grunt.loadNpmTasks('grunt-include-replace');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask('docFr', ['includereplace:docfr']);
  grunt.registerTask('minScript', ['uglify:minScript']);
  grunt.registerTask('default', ['includereplace:docfr','uglify:minScript']);
};
