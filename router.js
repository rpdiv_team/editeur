exports.load = function(app,express, config, databases, models){

  console.log('Loading editor plugin.');
  var editor=express();
  var prefix= "/editor";
  if(config.editor){
    prefix = config.editor.path || prefix;
  }
  
  if (databases) {
    console.log("prefix", prefix);
    editor.get('/index.html', function(req, res, next) {
      console.log("editor index");
      res.sendFile(__dirname + '/view/index.html');
    });

    editor.get('/dev', function (req, res, next) {
      console.log("dev editor index");
      res.sendFile(__dirname + '/view/index-dev.html');
    });
  
  
    editor.get('/userFiches', function (req, res, next) {
      console.log('/editor/userFiches', req.user.name);
      databases.editor.db.fetchAll('SELECT uid,max_slot FROM core.users where name=?',
              [req.user.name], function (err, user, max_slot) {
        if (err) {
          console.log("err", err);
          res.sendStatus(412);
          return;
        }
        databases.editor.db.fetchAll('SELECT fid as id, name, descr as fiche_descr, slot FROM fiche.save_fiche where uid=? order by slot',
                [user[0].uid], function (err, data) {
          if (err) {
            console.log("err 2", err);
            res.sendStatus(412);
            return;
          }
          res.json({save: data, max_slot: user[0].max_slot});
        });
      });
    });

    editor.post('/saveFiche', function (req, res, next) {
      console.log('/editor/saveFiche', req.body, req.query);
      databases.editor.db.fetchAll("select uid,max_slot FROM core.users where name=?", [req.user.name], function (err, data) {
        console.log("fucking query", req.body, data[0].uid);
        if (err || req.body.slot > data[0].max_slot) {
          res.sendStatus(423);
          return;
        }
        req.body.data.obj = JSON.stringify(req.body.data.obj);
        if (req.body.id) {
          console.log("update");
          databases.editor.db.update('fiche.save_fiche', req.body.data, [["fid=?", req.body.id], ["uid=?", data[0].uid]], function (err) {
            if (err) {
              console.log("err", err)
              res.sendStatus(412);
              return;
            }
            console.log(data);
            res.sendStatus(200);
          });
        } else {
          console.log("add");
          req.body.data.uid = data[0].uid;
          databases.editor.db.insert('fiche.save_fiche', req.body.data, function (err) {
            if (err) {
              console.log("err", err)
              res.sendStatus(412);
              return;
            }
            console.log("ok");
            res.sendStatus(200);
          });
        }
      });
    });

    editor.get('/loadFiche', function (req, res, next) {
      console.log('/editor/loadFiche');
      databases.editor.db.fetchAll("select uid FROM core.users where name=?", [req.user.name], function (err, data) {
        if (err) {
          res.sendStatus(412);
        } else if (req.query.id) {
          console.log("param", req.query.id, data[0].uid);
          databases.editor.db.fetchAll('select fid as id, name, obj, descr, gallerie, slot from fiche.save_fiche where fid=? and (uid=? or gallerie=true)',
                  [req.query.id, data[0].uid], function (err, result) {
            if (err) {
              console.log("err", err);
              res.sendStatus(412);
              return;
            }
            res.json(result[0]);
          });
        } else {
          res.sendStatus(400);
        }
      });
    });
  } else {
    editor.get('/standalone', function(req, res, next) {
      console.log("editor standalone");
      res.sendFile(__dirname + '/view/standalone.html');
    });
  }
  
  app.use(prefix,editor);
  console.log('/public'+prefix);
  console.log(__dirname + '/public');
  app.use('/public/editor', express.static(__dirname + '/public', { maxAge: 0 }));
};
